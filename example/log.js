var log = require('../index')({ level: 'trace' });
var logNoColor = require('../index')({ level: 'trace', noColor: true });

log.trace('trace log');
log.debug('debug log');
log.info('info log');
log.warn('warn log');
log.error('error log');
log.fatal('fatal log');
log.success('success log');
logNoColor.trace('trace log');
logNoColor.debug('debug log');
logNoColor.info('info log');
logNoColor.warn('warn log');
logNoColor.error('error log');
logNoColor.fatal('fatal log');
logNoColor.success('success log');