var logger = require('console-log-level');
var util = require('util');

if (!String.prototype.padEnd) {
  String.prototype.padEnd = function padEnd(targetLength, padString) {
    targetLength = targetLength >> 0; // floor if number or convert non-number to 0;
    padString = String(padString || ' ');
    if (this.length > targetLength) {
      return String(this);
    } else {
      targetLength = targetLength - this.length;
      if (targetLength > padString.length) {
        padString += padString.repeat(targetLength / padString.length); // append to original to ensure we are longer than needed
      }
      return String(this) + padString.slice(0, targetLength);
    }
  };
}

module.exports = function(options) {
  options = options || {};

  var symbolMap = process.platform === 'win32' ? {
    trace: '',
    debug: '',
    info: 'i',
    success: '√',
    warn: '‼',
    error: '×',
    fatal: '×'
  } : {
    trace: '',
    debug: '',
    info: 'ℹ',
    success: '✔',
    warn: '⚠',
    error: '✖',
    fatal: '✖'
  };

  var defaultTextDecor = text => text;

  var chalk = options.noColor === true ? null : require('chalk');

  var levelTextDecor = options.noColor === true ? {
    trace: defaultTextDecor,
    debug: defaultTextDecor,
    info: defaultTextDecor,
    warn: defaultTextDecor,
    error: defaultTextDecor,
    fatal: defaultTextDecor,
    success: defaultTextDecor,
  } : {
    trace: chalk.magenta.dim,
    debug: chalk.cyan,
    info: chalk.blue,
    warn: chalk.yellow,
    error: chalk.red,
    fatal: chalk.red.bold,
    success: chalk.green,
  }

  var log = logger({
    level: options.level,
    prefix: function(level) {
      var date = new Date().toISOString();

      var levelText = ('[' + level.toUpperCase() + ']').padEnd(9);
      var symbol = symbolMap[level];

      if (options.noColor !== true) {
        date = chalk.gray(date);
        levelText = levelTextDecor[level](levelText);
        symbol = levelTextDecor[level](symbol);
      }

      return date + ' ' + levelText + (symbol ? ' ' + symbol : '');
    }
  });

  log.success = function() {
    var level = 'success';
    var date = new Date().toISOString();
    var levelText = ('[' + level.toUpperCase() + ']').padEnd(9);
    var symbol = symbolMap[level];

    if (options.noColor !== true) {
      date = chalk.gray(date);
      levelText = levelTextDecor[level](levelText);
      symbol = levelTextDecor[level](symbol);
    }

    return console.log(
      date,
      levelText,
      symbol,
      util.format.apply(util, arguments)
    );
  };

  return log;
}